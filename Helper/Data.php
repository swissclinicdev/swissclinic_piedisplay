<?php

namespace Swissclinic\PieDisplay\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const XML_PATH_SWISSCLINIC_PIEDISPLAY_CONFIG= 'swissclinic/piedisplay_config/enable';
    const XML_PATH_SWISSCLINIC_PIEDISPLAY_MULTIPLIER= 'swissclinic/piedisplay_config/multiplier';

    /**
     * Data constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * @param null $storeId
     * @return mixed
     */
    public function isEnabled($storeId = null)
    {
        return $this->scopeConfig->isSetFlag(
            self::XML_PATH_SWISSCLINIC_PIEDISPLAY_CONFIG,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
    /**
     * @param null $storeId
     * @return mixed
     */
    public function getMultiplier($storeId = null)
    {
        return $this->scopeConfig->getValue(
            self::XML_PATH_SWISSCLINIC_PIEDISPLAY_MULTIPLIER,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

}