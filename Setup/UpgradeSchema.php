<?php

namespace Swissclinic\PieDisplay\Setup;

class UpgradeSchema implements \Magento\Framework\Setup\UpgradeSchemaInterface
{
    public function upgrade(\Magento\Framework\Setup\SchemaSetupInterface $setup,
                            \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.1', '<=')) {

            $setup->getConnection()->addColumn(
                $setup->getTable('eav_attribute'),
                'as_pie',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'default' => 0,
                    'nullable' => false,
                    'comment' => 'Is_pie attribute option setting'
                ]
            );
        }

        $setup->endSetup();
    }
}