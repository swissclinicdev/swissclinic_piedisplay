<?php


Namespace Swissclinic\PieDisplay\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Config\Model\Config\Source\Yesno;


class ProductAttributeFormbuild implements ObserverInterface  {

    protected $_yesNo;
    protected $_coreRegistry;
    protected $_hlp;

    public function __construct(
        Yesno $yesNo,
        \Magento\Framework\Registry $registry,
        \Swissclinic\PieDisplay\Helper\Data $hlp
        )
    {
        $this->_yesNo = $yesNo;
        $this->_coreRegistry = $registry;
        $this->_hlp =  $hlp;

    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $_enabled = $this->_hlp->isEnabled();

        if(!$_enabled){
            return $observer;
        }

        /** @var  $form \Magento\Backend\Block\Widget\Form\Generic */
        $form = $observer->getEvent()->getForm();
        $yesno = $this->_yesNo->toOptionArray();
        $_attributeValue = $this->getAttributeObject()->getData('as_pie');


        $form->addField(
            'as_pie',
            'select',
            [
                'name' => 'as_pie',
                'label' => __('Display as a pie'),
                'title' => __('Display as pie the value of this attribute'),
                'note' => __('Display as pie the value of this attribute'),
                'values' => $yesno,
                'value' => $_attributeValue
            ]
        );
        return $this;
    }
    private function getAttributeObject()
    {
        return $this->_coreRegistry->registry('entity_attribute');
    }



}