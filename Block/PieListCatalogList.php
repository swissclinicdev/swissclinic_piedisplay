<?php

namespace Swissclinic\PieDisplay\Block;

class PieListCatalogList extends \Magento\Catalog\Block\Product\ListProduct {

    const TOOLTIP_PREFIX = '_tooltip';

    /**
     * @var Swissclinic\PieDisplay\Helper\Data
     */
    protected $_shlp;

    /**
     * PieList constructor.
     * @param \Magento\Catalog\Block\Product\Context $context,
     * @param \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
     * @param \Magento\Catalog\Model\Layer\Resolver $layerResolver,
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
     * @param \Magento\Framework\Url\Helper\Data $urlHelper,
     * @param array $data = [],
     * @param \Swissclinic\PieDisplay\Helper\Data $shlp
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        \Magento\Catalog\Model\Layer\Resolver $layerResolver,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Url\Helper\Data $urlHelper,
        array $data = [],
        \Swissclinic\PieDisplay\Helper\Data $shlp
    ){

        $this->_shlp = $shlp;

        parent::__construct(
            $context,
            $postDataHelper,
            $layerResolver,
            $categoryRepository,
            $urlHelper,
            $data
        );
    }

    /**
     * Get Pie Data
     *
     * @return array
     */
    public function getPieAttributes() {
        $data = [];
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        $_multiplier = $this->_shlp->getMultiplier();

        foreach ($attributes as $_attribute) {

            if ($_attribute->getIsVisibleOnFront() && $_attribute->getData('as_pie')==1) {
                $value = $_attribute->getFrontend()->getValue($product);

                if ($value instanceof Phrase) {
                    $value = (string)$value;
                }

                if (is_string($value) && strlen($value)) {

                    $_attributeTooltip = $_attribute->getAttributeCode().self::TOOLTIP_PREFIX; // For example anti-age_tooltip
                    $_tooltip = ($product->getData($_attributeTooltip)) ? $product->getData($_attributeTooltip) : '';
                    $_displayValue = floatval($value);
                    $value = floatval($value) * floatval($_multiplier);


                    $data[$_attribute->getAttributeCode()] = [
                        'label' => __($_attribute->getStoreLabel()),
                        'value' => $value,
                        'displayValue' => $_displayValue,
                        'code' => $_attribute->getAttributeCode(),
                        'tooltip'=> $_tooltip
                    ];
                }
            }
        }
        return $data;
    }
}