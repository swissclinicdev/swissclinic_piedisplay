<?php

namespace Swissclinic\PieDisplay\Block;

class PieList extends \Magento\Catalog\Block\Product\View {
    const TOOLTIP_PREFIX = '_tooltip';

    /**
     * @var Swissclinic\PieDisplay\Helper\Data
     */
    protected $_shlp;

    /**
     * PieList constructor.
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\Url\EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Catalog\Helper\Product $productHelper
     * @param \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig
     * @param \Magento\Framework\Locale\FormatInterface $localeFormat
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param array $data
     * @param \Swissclinic\PieDisplay\Helper\Data $shlp
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\Url\EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Catalog\Helper\Product $productHelper,
        \Magento\Catalog\Model\ProductTypes\ConfigInterface $productTypeConfig,
        \Magento\Framework\Locale\FormatInterface $localeFormat,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        array $data = [],
        \Swissclinic\PieDisplay\Helper\Data $shlp
    ){

        $this->_shlp = $shlp;

        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data);
    }

    public function getAttributes(){

        $data = [];
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        $_multiplier = $this->_shlp->getMultiplier();


        foreach ($attributes as $_attribute) {
            $_displayValue = '';

            if ($_attribute->getIsVisibleOnFront() && $_attribute->getData('as_pie')==1) {
                $value = $_attribute->getFrontend()->getValue($product);

                if ($value instanceof Phrase) {
                    $value = (string)$value;
                }

                if (is_string($value) && strlen($value)) {

                    $_attributeTooltip = $_attribute->getAttributeCode().self::TOOLTIP_PREFIX; // For example anti-age_tooltip
                    $_tooltip = ($product->getData($_attributeTooltip)) ? $product->getData($_attributeTooltip) : '';
                    $_displayValue = floatval($value);
                    $value = floatval($value) * floatval($_multiplier);


                    $data[$_attribute->getAttributeCode()] = [
                        'label' => __($_attribute->getStoreLabel()),
                        'value' => $value,
                        'displayValue' => $_displayValue,
                        'code' => $_attribute->getAttributeCode(),
                        'tooltip'=> $_tooltip
                    ];
                }
            }
        }
        return $data;
    }
}